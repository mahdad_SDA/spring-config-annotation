package com.sda.tutorials.demo;

public interface Coach {
    String getDailyWorkout();
    String getDailyDiet();
}
